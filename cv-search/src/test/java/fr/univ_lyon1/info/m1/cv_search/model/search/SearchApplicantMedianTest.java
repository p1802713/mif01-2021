package fr.univ_lyon1.info.m1.cv_search.model.search;

import org.junit.jupiter.api.Test;

import java.util.Vector;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;

public class SearchApplicantMedianTest {

    @Test
    public void testStratMedianWithSuperiorComparatorAndValueTo40() {

        SearchApplicantMedian a = new SearchApplicantMedian();

        Vector<String> skills = new Vector<>(); //les compétences choisies
        skills.add("c");
        skills.add("PHP");

        Vector<String> resultat = a.search(">", 40, skills);

        boolean John = false;
        for (String i : resultat) {
            if (i.contains("John")) {
                John = true;
                assertThat(true, is(i.contains("45")));
            } else {
                assertThat(false, is(i.contains("Foo")));
                assertThat(false, is(i.contains("Jhon")));
                assertThat(false, is(i.contains("Maureen")));
            }
        }
        assertThat(true, is(John));
    }





    @Test
    public void testStratMedianWithInferiorOrEqualComparatorAndValueTo25() {

        SearchApplicantMedian a = new SearchApplicantMedian();

        Vector<String> skills = new Vector<>(); //les compétences choisies
        skills.add("c");

        Vector<String> resultat = a.search("<=", 25, skills);

        boolean Maureen = false;
        for (String i : resultat) {
            if (i.contains("Maureen")) {
                Maureen = true;
                assertThat(true, is(i.contains("10")));
            } else {
                assertThat(false, is(i.contains("John")));
                assertThat(false, is(i.contains("Foo")));
                assertThat(false, is(i.contains("Sandrine")));
            }
            assertThat(true, is(Maureen));
        }
    }
}