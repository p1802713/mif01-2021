package fr.univ_lyon1.info.m1.cv_search.controller;

import fr.univ_lyon1.info.m1.cv_search.model.Strategy;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.jupiter.api.Test;


import java.util.Vector;

public class ControllerTest {

    @Test
    public void testAddSkill() {

        Controller c = new Controller();

        c.addSkill("java");
        c.addSkill("");
        Vector<String> s = c.getSkillsController();

        assertThat(true, is(s.contains("java")));
        assertThat(1, is(s.size()));
    }


    @Test
    public void testDeleteSkillTest() {

        Controller c = new Controller();

        c.addSkill("python");
        c.addSkill("java");
        c.addSkill("");

        c.deleteSkill("java");
        c.deleteSkill("c++");
        Vector<String> s = c.getSkillsController();

        assertThat(1, is(s.size()));
    }


    @Test
    public void testSetSelectedChampsTest() {
        Controller c = new Controller();

        String champ = "all >=50";
        c.setSelectedChamp(champ);

        assertThat(champ , is(Strategy.getInstance().getSelectedChamp()));
    }

}
