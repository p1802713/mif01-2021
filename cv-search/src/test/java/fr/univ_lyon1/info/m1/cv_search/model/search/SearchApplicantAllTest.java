package fr.univ_lyon1.info.m1.cv_search.model.search;

import org.junit.jupiter.api.Test;

import java.util.Vector;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class SearchApplicantAllTest {


    @Test
    public void testStratAllWithSuperiorOrEqualComparatorAndValueTo50() {

        SearchApplicantAll a = new SearchApplicantAll();

        Vector<String> skills = new Vector<>(); //les compétences choisies
        skills.add("python");
        skills.add("c++");
        skills.add("java");

        Vector<String> resultat = a.search(">=", 50, skills);

        boolean MmeFelti = false;
        for (String i : resultat) {
            if (i.contains("Sandrine")) {
                MmeFelti = true;
            } else {
                assertThat(false, is(i.contains("Foo")));
                assertThat(false, is(i.contains("Jhon")));
                assertThat(false, is(i.contains("Maureen")));
            }
        }
        assertThat(true, is(MmeFelti));
    }


    @Test
    public void testStratAllWithSuperiorComparatorAndValueTo70() {

        SearchApplicantAll a = new SearchApplicantAll();

        Vector<String> skills = new Vector<>(); //les compétences choisies
        skills.add("c");
        skills.add("c++");

        Vector<String> result = a.search(">", 70, skills);

        for (String i : result) {
            assertThat(false, is(i.contains("Foo")));
            assertThat(false, is(i.contains("Sandrine")));
            assertThat(false, is(i.contains("Maureen")));
        }
    }
}
