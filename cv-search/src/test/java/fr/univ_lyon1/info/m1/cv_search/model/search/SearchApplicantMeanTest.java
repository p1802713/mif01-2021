package fr.univ_lyon1.info.m1.cv_search.model.search;

import fr.univ_lyon1.info.m1.cv_search.model.applicant.Applicant;
import fr.univ_lyon1.info.m1.cv_search.model.applicant.ApplicantList;
import fr.univ_lyon1.info.m1.cv_search.model.applicant.ApplicantListBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.Vector;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;

public class SearchApplicantMeanTest {

    @Test
     public void testStratMeanWithSuperiorComparatorAndValueTo50() {

        SearchApplicantMean a = new SearchApplicantMean();

        Vector<String> skills = new Vector<>(); //les compétences choisies
        skills.add("c");
        skills.add("java");

        Vector<String> resultat = a.search(">", 50, skills);

        boolean John = false;
        for (String i : resultat) {
            if (i.contains("John")) {
                John = true;
                assertThat(true, is(i.contains("70")));
            } else {
                assertThat(false, is(i.contains("Foo")));
                assertThat(false, is(i.contains("Jhon")));
                assertThat(false, is(i.contains("Maureen")));
            }
        }
        assertThat(true, is(John));
    }





    @Test
    public void testStratMeanWithInferiorComparatorAndValueTo10() {

        SearchApplicantMean a = new SearchApplicantMean();

        Vector<String> skills = new Vector<>(); //les compétences choisies
        skills.add("c");

        Vector<String> resultat = a.search("<", 10, skills);


        for (String i : resultat) {
            assertThat(false, is(i.contains("John")));
            assertThat(false, is(i.contains("Foo")));
            assertThat(false, is(i.contains("Sandrine")));
            assertThat(false, is(i.contains("Maureen")));
        }
    }

}

