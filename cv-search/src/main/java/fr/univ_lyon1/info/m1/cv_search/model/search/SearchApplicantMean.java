package fr.univ_lyon1.info.m1.cv_search.model.search;

import fr.univ_lyon1.info.m1.cv_search.model.applicant.Applicant;
import fr.univ_lyon1.info.m1.cv_search.model.applicant.ApplicantListBuilder;
import fr.univ_lyon1.info.m1.cv_search.model.applicant.ApplicantList;
import fr.univ_lyon1.info.m1.cv_search.model.sort.SortListApplicantDescending;
import java.util.Vector;
import java.io.File;

public class SearchApplicantMean implements SearchApplicant {

    /**
     * Return the result of the search.
     */
    public Vector<String> search(String comparateur, int nombre, Vector<String> skills) {
        ApplicantList listApplicants
                        = new ApplicantListBuilder(new File(".")).build();
        Vector<String> names = new Vector<String>();
        Vector<Float> lsskills = new Vector<Float>();
        Vector<Integer> lsexperiences = new Vector<Integer>();
        SortListApplicantDescending sortLs = new SortListApplicantDescending();
        for (Applicant a : listApplicants) {
            Float mean = a.getMeanSelectedSkill(skills);
            switch (comparateur) {
                case "<":
                    if (mean < nombre) {
                        int pos;
                        pos = sortLs.sort(lsskills, lsexperiences, mean, a.getExperience(skills));
                        lsskills.insertElementAt(mean, pos);
                        lsexperiences.insertElementAt(a.getExperience(skills), pos);
                        names.insertElementAt(a.getName() + " mean = " 
                        + String.valueOf(mean) + a.getExperiences(skills), pos);
                    }   
                    break;
                case ">":
                    if (mean > nombre) {
                        int pos;
                        pos = sortLs.sort(lsskills, lsexperiences, mean, a.getExperience(skills));
                        lsskills.insertElementAt(mean, pos);
                        lsexperiences.insertElementAt(a.getExperience(skills), pos);
                        names.insertElementAt(a.getName() + " mean = " 
                        + String.valueOf(mean) + a.getExperiences(skills), pos);
                    } 
                    break;
                case "=":
                    if (mean == nombre) {
                        int pos;
                        pos = sortLs.sort(lsskills, lsexperiences, mean, a.getExperience(skills));
                        lsskills.insertElementAt(mean, pos);
                        lsexperiences.insertElementAt(a.getExperience(skills), pos);
                        names.insertElementAt(a.getName() + " mean = " 
                        + String.valueOf(mean) + a.getExperiences(skills), pos);
                    }
                    break;
                case "<=":
                    if (mean <= nombre) {
                        int pos;
                        pos = sortLs.sort(lsskills, lsexperiences, mean, a.getExperience(skills));
                        lsskills.insertElementAt(mean, pos);
                        lsexperiences.insertElementAt(a.getExperience(skills), pos);
                        names.insertElementAt(a.getName() + " mean = " 
                        + String.valueOf(mean) + a.getExperiences(skills), pos);
                    }
                    break;
                case ">=":
                    if (mean >= nombre) {
                        int pos;
                        pos = sortLs.sort(lsskills, lsexperiences, mean, a.getExperience(skills));
                        lsskills.insertElementAt(mean, pos);
                        lsexperiences.insertElementAt(a.getExperience(skills), pos);
                        names.insertElementAt(a.getName() + " mean = " 
                        + String.valueOf(mean) + a.getExperiences(skills), pos);
                    }
                    break;
                default:
                    break;
            }

        }
        return names;
    }
}
