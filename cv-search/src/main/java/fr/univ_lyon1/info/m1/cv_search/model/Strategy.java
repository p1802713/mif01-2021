package fr.univ_lyon1.info.m1.cv_search.model;

import java.util.Vector;

public final class Strategy {
    private static volatile Strategy instance = null;
    private int nbrValue;
    private Vector<String> champ;
    private String selectedChamp;

    /**
     * constructor of Strategy.
     */
    private Strategy() {
        super();
        nbrValue = 0;
        selectedChamp = "";
        champ = new Vector<String>();
    }

    public static Strategy getInstance() {
        if (Strategy.instance == null) {
            synchronized (Strategy.class) {
                if (Strategy.instance == null) {
                    Strategy.instance = new Strategy();
                }
            }
        }
        return Strategy.instance;
    }

    /**
     * Add a strategy.
     */
    public void addStrategy(Integer value, String comparateur, String strat) {
        nbrValue++;
        String toAdd = getNameCase(value, comparateur, strat);
        champ.add(toAdd);
    }

    /**
     * Get the name for combobox champ.
     */
    public String getNameCase(int val, String comp, String strat) {
        String fullChamp = comp + String.valueOf(val);
        switch (strat) {
            case "mean":
                fullChamp = "average " + fullChamp;
            break;
            case "all":
                fullChamp = "all " + fullChamp;
            break;
            case "median":
                fullChamp = "median " + fullChamp;
            break;
            default:
            break;
        }
        return fullChamp;
    }
    
    /**
     * Accesseur of nbrvalue.
     */
    public int getNbrValue() {
        return nbrValue;
    }

    /**
     * Accesseur of selectedChamp.
     */
    public String getSelectedChamp() {
        return selectedChamp;
    }

    /**
     * Mutateur of selectedChamp.
     */
    public void setSelectedChamp(String champ) {
        selectedChamp = champ;
    }

    /**
     * Mutateur of a champ.
     */
    public String getChamp(int index) {
        return champ.get(index);
    }

    /**
     * Mutateur of a champBis.
     */
    public Vector<String> getChamp() {
        return champ;
    }

    /**
     * return only the comparator of the champ.
     */
    public String getComparateur(String champ) {
        String result  = champ.replaceAll("[0-9]", "");
        result = result.replaceAll("average ", "");
        result = result.replaceAll("all ", "");
        result = result.replaceAll("median ", "");
        return result;
    }

    /**
     * return the selected method of the selected champ.
     */
    public String getMethod(String champ) {
        if (champ.startsWith("all ")) {
            return "all";
        } else {
            if (champ.startsWith("average ")) {
                return "average";
            } else {
                return "median";
            }
            
        }
    }

    /**
     * return the number of the selected champ.
     */
    public int getValue(String method, String comparateur, String champ) {
        String result  = champ.replaceAll(comparateur, "");
        result = result.replaceAll(method, "");
        result = result.replaceAll(" ", "");
        return Integer.parseInt(result);
    }
}
