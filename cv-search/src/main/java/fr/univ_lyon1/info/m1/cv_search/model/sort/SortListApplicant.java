package fr.univ_lyon1.info.m1.cv_search.model.sort;

import java.util.Vector;

public interface SortListApplicant {
    int sort(Vector<Float> skills, Vector<Integer> experiences, Float skill, int experience);
}
