package fr.univ_lyon1.info.m1.cv_search.model.search;

import java.util.Vector;

public interface SearchApplicant {
     Vector<String> search(String comparateur, int nombre, Vector<String> skills);
}
