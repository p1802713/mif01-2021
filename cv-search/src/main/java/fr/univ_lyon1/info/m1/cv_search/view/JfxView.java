package fr.univ_lyon1.info.m1.cv_search.view;

import java.util.Vector;
import fr.univ_lyon1.info.m1.cv_search.controller.Controller;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.geometry.Pos;
import javafx.geometry.Insets;
import javafx.stage.Stage;
import javafx.scene.text.Font;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;


public class JfxView implements PropertyChangeListener {
    private HBox searchSkillsBox;
    private VBox resultBox;
    private Controller controle;

    /**
     * Create the main view of the application.
     */
    public JfxView(Controller controller, Stage stage, int width, int height) {
        // Name of window
        stage.setTitle("Search for CV");

        this.controle = controller;
        controle.addPropretyChangeListener(this::propertyChange);

        VBox root = new VBox();
        root.setPadding(new Insets(20, 20, 20, 20));
        root.setSpacing(10);

        Label label = new Label("Search for CV");
        label.setFont(new Font("Arial", 20));

        Node newSkillBox = createNewSkillBox();
        Node searchSkillBox = createCurrentSearchSkillsWidget();
        Node resultComboBox = createStrategyWidget();
        Node search = createSearchWidget();
        Node resultBox = createResultsWidget();
        root.getChildren().add(label);
        root.getChildren().add(newSkillBox);
        root.getChildren().add(searchSkillBox);
        root.getChildren().add(resultComboBox);
        root.getChildren().add(search);
        root.getChildren().add(resultBox);


        // Everything's ready: add it to the scene and display it
        Scene scene = new Scene(root, width, height);
        stage.setScene(scene);
        stage.show();
    }

    private Node createNewSkillBox() {
        HBox newSkillBox = new HBox();
        Label labelSkill = new Label(" Skill : ");
        TextField textField = new TextField();
        Button submitButton = new Button("add");
        submitButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                controle.addSkill(textField.getText().strip());
            }
        });
        newSkillBox.getChildren().addAll(labelSkill, textField, submitButton);
        return newSkillBox;
    }

    private Node createCurrentSearchSkillsWidget() {
        searchSkillsBox = new HBox();
        return searchSkillsBox;
    }

    private Node createStrategyWidget() {
        HBox newStrategyBox = new HBox();
        Label label = new Label(" Strategy : ");

        final ComboBox comboBox = new ComboBox();
        Vector<String> champs = controle.getChamp();
        for (String champ : champs) {
            comboBox.getItems().addAll(champ);
        }
        comboBox.setPromptText("Select a strategy"); 
        comboBox.setEditable(true);
        comboBox.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                controle.setSelectedChamp(comboBox.getValue().toString());
            }
        });
        newStrategyBox.getChildren().addAll(label, comboBox);
        return newStrategyBox;
    }

    private Node createSearchWidget() {
        Button search = new Button("Search");
        search.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                controle.searchResult();
            }
        });
        return search;
    }

    private Node createResultsWidget() {
        resultBox = new VBox();
        return resultBox;
    }

    public void propertyChange(final PropertyChangeEvent event) {
        switch (event.getPropertyName()) {
            case "search":
                resultBox.getChildren().clear();
                Vector<String> names = (Vector<String>) event.getNewValue();
                for (String name : names) {
                    HBox box = new HBox();
                    Label label = new Label(name);
                    box.getChildren().add(label);
                    box.setStyle("-fx-padding: 2;" + "-fx-border-style: solid inside;"
                        + "-fx-border-width: 1;" + "-fx-border-insets: 5;"
                        + "-fx-border-radius: 5;" + "-fx-border-color: black;");
                    box.setAlignment(Pos.BASELINE_CENTER);
                    resultBox.getChildren().add(box);
                }
            break;
            case "skills":
                searchSkillsBox.getChildren().clear();
                resultBox.getChildren().clear();
                Vector<String> skills = (Vector<String>) event.getNewValue();
                for (String skill : skills) {
                    HBox box = new HBox();
                    Label label = new Label(skill);
                    Button skillBtn = new Button("x");
                    skillBtn.setStyle("-fx-text-fill: #FF0000");
                    box.setStyle("-fx-padding: 2;" + "-fx-border-style: solid inside;"
                        + "-fx-border-width: 1;" + "-fx-border-insets: 5;"
                        + "-fx-border-radius: 5;" + "-fx-border-color: black;");
                    box.setAlignment(Pos.BASELINE_CENTER);
                    box.getChildren().addAll(label, skillBtn);
                    searchSkillsBox.getChildren().add(box);
                    skillBtn.setOnAction(new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent event) {
                            controle.deleteSkill(skill);
                        }
                    });
                }
            break;
            default:
            break;
        }
    }
}
