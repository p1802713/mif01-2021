package fr.univ_lyon1.info.m1.cv_search.model.applicant;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Vector;

import java.util.Map;

public class Applicant {
    private Map<String, Integer> skills = new HashMap<>();
    private String name;
    private Map<String, Integer> experience = new HashMap<>();

    public int getSkill(String string) {
        return skills.getOrDefault(string, 0);
    }

    public void setSkill(String string, int value) {
        skills.put(string, value);
    }

    public void setExperience(String string, int value) {

        if (experience.containsKey(string)) {
            int years = experience.get(string);
            experience.remove(string);
            years = years + value;
            experience.put(string, years);
        } else {
            experience.put(string, value); 
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getMeanSelectedSkill(Vector<String> skills) {
        float mean = 0;
        for (String skill: skills) {
            mean = getSkill(skill) + mean;
        }
        mean = mean / skills.size();
        return mean;
    }

    public Double getMedianSelectedSkill(Vector<String> skills) {
        if (skills.size() == 0) {
            return 0.0;
        } else {
            if (skills.size() == 1) {
                return (double) getSkill(skills.get(0));
            } else {
                int[] valueSkills = new int[skills.size()];
                double result = 0.0;
                for (int i = 0; i < skills.size(); i++) {
                    valueSkills[i] = getSkill(skills.get(i));
                }
                Arrays.sort(valueSkills);
                if (valueSkills.length % 2 == 0) {
                    result = ((double) valueSkills[valueSkills.length / 2]
                        + (double) valueSkills[valueSkills.length / 2 - 1]) / 2;
                } else {
                    result = (double) valueSkills[valueSkills.length / 2];
                }
                return result;
            }
        } 
    }

    public int getExperience(String skill) {
        return experience.getOrDefault(skill, 0);
    }

    public int getExperience(Vector<String> skills) {
        int result = 0;
        for (String skill: skills) {
            result += getExperience(skill);
        }
        return result;
    }

    public String getExperiences(Vector<String> skills) {
        String result = "";
        for (String skill: skills) {
            result = "\n" + "With: " + getExperience(skill) + " years of experience in "
                    + skill + result;
        }
        return result;
    }
}
