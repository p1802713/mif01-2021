package fr.univ_lyon1.info.m1.cv_search.model.applicant;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Map;

import org.yaml.snakeyaml.Yaml;

public class ApplicantBuilder {

    private File file;

    public ApplicantBuilder(File f) {
        this.file = f;
    }

    public ApplicantBuilder(String filename) {
        this.file = new File(filename);
    }

    /**
     * Build the applicant from the Yaml file provided to the constructor.
     */
    public Applicant build() {
        Applicant a = new Applicant();
        Yaml yaml = new Yaml();
        Map<String, Object> map;
        try {
            map = yaml.load(new FileInputStream(file));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw new Error(e);
        }

        a.setName((String) map.get("name"));

        // Cast may fail if the Yaml is incorrect. Ideally we should provide
        // clean error messages.
        @SuppressWarnings("unchecked")
        Map<String, Integer> skills = (Map<String, Integer>) map.get("skills");

        for (String skill : skills.keySet()) {
            Integer value = skills.get(skill);
            a.setSkill(skill, value);
        }


        Map<String, Object> company = (Map<String, Object>) map.get("experience");

        for (String comp : company.keySet()) {
            Map<String, Object> element = (Map<String, Object>) company.get(comp);
            int start = (int) element.get("start");
            int end = (int) element.get("end");
            int difference = end - start;
            ArrayList<String> values = (ArrayList<String>) element.get("keywords");
            for (String i : values) {
                a.setExperience(i, difference);
            }
        }
        return a;
    }
}
