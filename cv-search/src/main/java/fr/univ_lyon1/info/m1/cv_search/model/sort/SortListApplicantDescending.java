package fr.univ_lyon1.info.m1.cv_search.model.sort;

import java.util.concurrent.ThreadLocalRandom;
import java.util.Vector;

public class SortListApplicantDescending implements SortListApplicant {

    public int sort(Vector<Float> skills, Vector<Integer> experiences,
    Float skill, int experience) {
        int position = 0;
        boolean notFinish = true;
        while (notFinish && position < skills.size()) {
            if (skill > skills.get(position)) {
                notFinish = false;
            } else {
                if (skill - skills.get(position) == 0.0) {
                    if (experience - experiences.get(position) > 0) {
                        notFinish = false;
                    } else {
                        if (experience - experiences.get(position) == 0) {
                            position = position + ThreadLocalRandom.current().nextInt(0, 2);
                            notFinish = false;
                        } else {
                            if (experience - experiences.get(position) > 0) {
                                notFinish = false;
                            } else {
                                position++;
                            }
                        }  
                    }
                } else {
                    position++;
                }
            }
        }
        if (position <= skills.size()) {
            return position;
        } else {
            return skills.size();
        }
        
    }
}
