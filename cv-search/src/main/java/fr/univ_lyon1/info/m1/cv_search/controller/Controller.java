package fr.univ_lyon1.info.m1.cv_search.controller;

import fr.univ_lyon1.info.m1.cv_search.model.Strategy;
import fr.univ_lyon1.info.m1.cv_search.model.search.SearchApplicantAll;
import fr.univ_lyon1.info.m1.cv_search.model.search.SearchApplicantMean;
import fr.univ_lyon1.info.m1.cv_search.model.search.SearchApplicantMedian;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Vector;

public class Controller {
    private PropertyChangeSupport notif;
    private Vector<String> skills;
    private Vector<String> names;

    /**
     * Create the controller of the application.
     */
    public Controller() {
        Strategy.getInstance().addStrategy(10, "<", "mean");
        Strategy.getInstance().addStrategy(50, ">=", "all");
        Strategy.getInstance().addStrategy(50, ">", "mean");
        Strategy.getInstance().addStrategy(40, ">", "median");
        notif = new PropertyChangeSupport(this);
        skills = new Vector<String>();
        names = new Vector<String>();
    }

    public Vector<String> getChamp() {
        return Strategy.getInstance().getChamp();
    }

    public void deleteSkill(String skill) {
        skills.remove(skill);
        notif.firePropertyChange("skills", "", skills);
    }

    public void searchResult() {
        String selectedStartegy = Strategy.getInstance().getSelectedChamp();
        if (selectedStartegy != "" && skills.size() != 0) {
            String comparateur = Strategy.getInstance().getComparateur(selectedStartegy);
            String method = Strategy.getInstance().getMethod(selectedStartegy);
            int value = Strategy.getInstance().getValue(method, comparateur, selectedStartegy);
            switch (method) {
                case "average":
                    SearchApplicantMean searchMo = new SearchApplicantMean();
                    names = searchMo.search(comparateur, value, skills);
                    break;
                case "all":
                    SearchApplicantAll searchA = new SearchApplicantAll();
                    names = searchA.search(comparateur, value, skills);
                    break;
                case "median":
                    SearchApplicantMedian searchM = new SearchApplicantMedian();
                    names = searchM.search(comparateur, value, skills);
                    break;
                default:
                    break;
            }
            notif.firePropertyChange("search", "", names);
        }
    }

    public void addSkill(String text) {
        if (text != "") {
            skills.add(text);
            notif.firePropertyChange("skills", "", skills);
        }
    }

    public void setSelectedChamp(String champ) {
        Strategy.getInstance().setSelectedChamp(champ);
    }

    public Vector<String> getSkillsController() {
        return skills;
    }

    public void addPropretyChangeListener(PropertyChangeListener l) {
        this.notif.addPropertyChangeListener(l);
    }
}
